import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfferRoutingModule } from './offer-routing.module';
import { OfferComponent } from './offer/offer.component';
import { OfferS1Component } from './offer-s1/offer-s1.component';
import { OfferS2Component } from './offer-s2/offer-s2.component';
import { OfferS3Component } from './offer-s3/offer-s3.component';


@NgModule({
  declarations: [
    OfferComponent,
    OfferS1Component,
    OfferS2Component,
    OfferS3Component
  ],
  imports: [
    CommonModule,
    OfferRoutingModule
  ]
})
export class OfferModule { }
