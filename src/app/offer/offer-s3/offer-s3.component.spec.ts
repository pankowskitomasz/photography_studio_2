import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferS3Component } from './offer-s3.component';

describe('OfferS3Component', () => {
  let component: OfferS3Component;
  let fixture: ComponentFixture<OfferS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
